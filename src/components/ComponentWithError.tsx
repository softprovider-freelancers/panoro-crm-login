//= Functions & Modules
// Others
import React from "react";

export type Props = React.PropsWithChildren<{
    error?: string;
}>;

/**
 * A component that, if the `error` prop is given, will display it under the children 
 *
 * @param {Props} props The component props
 * @param {React.ReactNode} props.children The children
 * @param {string} [props.error] The error to be displayed under it
 */
export default function ComponentWithError(props: Props) {
    // TODO
    return null;
}




