//= Functions & Modules
// Others
import React from "react";

//= Structures & Data 
// Own
import { Props as ComponentWithErrorProps } from './ComponentWithError';
import { Props as InputProps } from './Input';

//= React components
// Own
import ComponentWithError from './ComponentWithError';
import Input from './Input';

export type Props = InputProps & {
    error?: ComponentWithErrorProps["error"];
};

/**
 * A input component that also can display an icon in the right side
 *
 * @param {Props} props The component props. Accepting all <input/> props
 * @param {SvgrComponent} [props.icon] The icon to be displayed in the right side of the input
 */
export default function InputWithError({error, ...props}: Props) {
    return <ComponentWithError error={error}><Input {...props}/></ComponentWithError>;
}
