//= Functions & Modules
// Others
import React from "react";

//= Structures & Data
// Own
import { RegisterSectionProps } from "../../data/RegisterSectionProps";

type Props = RegisterSectionProps & {};

/**
 * The success registered section
 *
 * @param {Props} props The component props
 * @param {(data: any) => void} props.onFinish Handler to call when the section is finished
 */
export default function SuccessRegisteredSection(props: Props) {
    // TODO
    return null;
}

