//= Functions & Modules
// Others
import React from "react";

//= Structures & Data
// Own
import { RegisterSectionProps } from "../../data/RegisterSectionProps";

type Props = RegisterSectionProps & {};

/**
 * The modules section of register page where user can see the selected modules
 *
 * @param {Props} props The component props
 * @param {(data: any) => void} props.onFinish Handler to call when the section is finished
 */
export default function ModulesSection(props: Props) {
    // TODO
    return null;
}
