//= Functions & Modules
// Others
import React from "react";

//= Structures & Data
// Own
import { RegisterSectionProps } from "../../data/RegisterSectionProps";

type Props = RegisterSectionProps & {};

/**
 * The basic register form section
 *
 * @param {Props} props The component props
 * @param {(data: any) => void} props.onFinish Handler to call when the section is finished
 */
export default function BasicFieldsFormSection(props: Props) {
    // TODO
    return null;
}






