//= Functions & Modules
// Others
import React from "react";

//= React components
// Own
import {SvgrComponent} from '../data/SvgrComponent';

export type Props = React.ReactHTMLElement<HTMLInputElement> & {
    icon?: SvgrComponent;
};

/**
 * A input component that also can display an icon in the right side
 *
 * @param {Props} props The component props. Accepting all <input/> props
 * @param {SvgrComponent} [props.icon] The icon to be displayed in the right side of the input
 */
export default function Input(props: Props) {
    // TODO
    return null;
}



