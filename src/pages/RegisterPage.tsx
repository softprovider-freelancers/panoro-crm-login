//= Functions & Modules
// Others
import React, {useRef, useState} from "react";

enum Step {
    BASIC_FIELDS = 0,
    BUSINESS_FIELDS = 1,
    MODULES = 2,
    SUCCESS = 3
}

const BasicFieldsFormSection = React.lazy(() => import("../components/register/BasicFieldsFormSection"));
const BusinessFieldsFormSection = React.lazy(() => import("../components/register/BusinessFieldsFormSection"));
const ModulesSection = React.lazy(() => import("../components/register/ModulesSection"));
const SuccessRegisteredSection = React.lazy(() => import("../components/register/SuccessRegisteredSection"));

/**
 * The register page
 */
export default function RegisterPage() {
    // Holding the current state of the register process
    const [currentStep, setCurrentStep] = useState<Step>(Step.BASIC_FIELDS);

    // Holding the current displayed step's component
    const currentComponent = useRef(BasicFieldsFormSection);

    // Holding the user introduced data
    const registerData = useRef({});

    const CurrentComponent = currentComponent.current; // <= Now you can use it like <CurrentComponent onFinish={(data: any) => { Object.assign(registerData.current, data); setCUrrentStep(...); currentComponent.current = ... }}/>

    // TODO
    return null;
}
